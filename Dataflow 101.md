# Dataflow 101

## Get the Pub/Sub emulator

Install emulator:

```
gcloud components install pubsub-emulator
gcloud components update
```

Export Pub/Sub Project ID:

```
export PUBSUB_PROJECT_ID=my-project-id
```

Start emulator:

```
gcloud beta emulators pubsub start --project=$PUBSUB_PROJECT_ID
```

Export emulator environment variable

```
$(gcloud beta emulators pubsub env-init)
```

## Create topic & subscription

Can't be done via gcloud commands, let's do it with a client library, we can download python one:

```
git clone git@github.com:GoogleCloudPlatform/python-docs-samples.git
cd python-docs-samples/pubsub/cloud-client
mkvirtualenv cloud-client
workon cloud-client
pip install -r requirements.txt
```

More info could be found [here](https://cloud.google.com/pubsub/docs/emulator#using_the_emulator).

```
python publisher.py $PUBSUB_PROJECT_ID create main_topic
python subscriber.py $PUBSUB_PROJECT_ID create main_topic main_subs
```

Checking that we have created them:

```
CLOUDSDK_API_ENDPOINT_OVERRIDES_PUBSUB=http://localhost:8085/ gcloud pubsub topics list
CLOUDSDK_API_ENDPOINT_OVERRIDES_PUBSUB=http://localhost:8085/ gcloud pubsub subscriptions list
```


## Publish messages

There's a magic environment variable which will help us pointing pubsub to our local emulator, it's called `CLOUDSDK_API_ENDPOINT_OVERRIDES_PUBSUB`, I don't even know how did a find it :confused::

```
export CLOUDSDK_API_ENDPOINT_OVERRIDES_PUBSUB=http://localhost:8085/
gcloud pubsub topics publish mainto_topic --message '{"foo": "aaaaa"}' --attribute 'dataset_id=one,table_id=two'
```

## Run Dataflow locally

First of all the requirements for running Dataflow on Java:

```
brew install maven

brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk/openjdk/adoptopenjdk8

export JAVA_HOME="/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home"

java -version
mvn -version
```

```
openjdk version "1.8.0_242"
OpenJDK Runtime Environment (AdoptOpenJDK)(build 1.8.0_242-b08)
OpenJDK 64-Bit Server VM (AdoptOpenJDK)(build 25.242-b08, mixed mode)
```

> I'd say it doesn't work with earlier versions of Java, read it somewhere, but I don't remember where

Google provide some templates but we need some with dynamic destinations, in this case dynamic dataset name, as tables remain static. Ryan McDowell has done part of the job for us here:

```
git clone git@github.com:ryanmcdowell/dataflow-bigquery-dynamic-destinations.git
```

In that repository he implements dynamic table name, which "isn't" our use case, but with dynamic dataset and tables we could have a generic bq streaming writter, that sounds cool :)

You can find that implementation here:

```
git@gitlab.com:emailmeter/dataflow-generic-bq-writer.git
```

As I didn't know how the hell could I run this locally, tried with the [docs](https://cloud.google.com/dataflow/docs/guides/specifying-exec-params) but setting those parameters didn't help:

```
Dataflow started
[WARNING]
java.lang.IllegalArgumentException: Unknown 'runner' specified 'DirectRunner', supported pipeline runners [DataflowRunner, TestDataflowRunner]
    at org.apache.beam.sdk.options.PipelineOptionsFactory.parseObjects (PipelineOptionsFactory.java:1664)
    at org.apache.beam.sdk.options.PipelineOptionsFactory.access$400 (PipelineOptionsFactory.java:115)
    at org.apache.beam.sdk.options.PipelineOptionsFactory$Builder.as (PipelineOptionsFactory.java:298)
    at com.google.cloud.pso.pipeline.PubsubToBigQueryDynamicDestinations.main (PubsubToBigQueryDynamicDestinations.java:125)
    at jdk.internal.reflect.NativeMethodAccessorImpl.invoke0 (Native Method)
    at jdk.internal.reflect.NativeMethodAccessorImpl.invoke (NativeMethodAccessorImpl.java:62)
    at jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke (DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke (Method.java:567)
    at org.codehaus.mojo.exec.ExecJavaMojo$1.run (ExecJavaMojo.java:282)
    at java.lang.Thread.run (Thread.java:830)
Caused by: java.lang.ClassNotFoundException: DirectRunner
    at java.net.URLClassLoader.findClass (URLClassLoader.java:436)
    at java.lang.ClassLoader.loadClass (ClassLoader.java:588)
    at java.lang.ClassLoader.loadClass (ClassLoader.java:521)
    at java.lang.Class.forName0 (Native Method)
    at java.lang.Class.forName (Class.java:416)
    at org.apache.beam.sdk.options.PipelineOptionsFactory.parseObjects (PipelineOptionsFactory.java:1650)
    at org.apache.beam.sdk.options.PipelineOptionsFactory.access$400 (PipelineOptionsFactory.java:115)
    at org.apache.beam.sdk.options.PipelineOptionsFactory$Builder.as (PipelineOptionsFactory.java:298)
    at com.google.cloud.pso.pipeline.PubsubToBigQueryDynamicDestinations.main (PubsubToBigQueryDynamicDestinations.java:125)
    at jdk.internal.reflect.NativeMethodAccessorImpl.invoke0 (Native Method)
    at jdk.internal.reflect.NativeMethodAccessorImpl.invoke (NativeMethodAccessorImpl.java:62)
    at jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke (DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke (Method.java:567)
    at org.codehaus.mojo.exec.ExecJavaMojo$1.run (ExecJavaMojo.java:282)
    at java.lang.Thread.run (Thread.java:830)
[INFO] ------------------------------------------------------------------------
[INFO] BUILD FAILURE
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  20.568 s
[INFO] Finished at: 2020-03-04T12:21:35+01:00
[INFO] ------------------------------------------------------------------------
[ERROR] Failed to execute goal org.codehaus.mojo:exec-maven-plugin:1.6.0:java (default-cli) on project dataflow-bigquery-dynamic-destinations: An exception occured while executing the Java class. Unknown 'runner' specified 'DirectRunner', supported pipeline runners [DataflowRunner, TestDataflowRunner] -> [Help 1]
[ERROR]
[ERROR] To see the full stack trace of the errors, re-run Maven with the -e switch.
[ERROR] Re-run Maven using the -X switch to enable full debug logging.
[ERROR]
[ERROR] For more information about the errors and possible solutions, please read the following articles:
[ERROR] [Help 1] http://cwiki.apache.org/confluence/display/MAVEN/MojoExecutionException
```

TL;DR:

```
An exception occured while executing the Java class. Unknown 'runner' specified 'DirectRunner', supported pipeline runners [DataflowRunner, TestDataflowRunner]
```

Spolier:
> Never surrender

There's a quickstart where you can run a [wordcount example](https://cloud.google.com/dataflow/docs/quickstarts/quickstart-java-maven#get-the-wordcount-code), if something works, copy it!

```
$ mvn archetype:generate \
      -DarchetypeGroupId=org.apache.beam \
      -DarchetypeArtifactId=beam-sdks-java-maven-archetypes-examples \
      -DarchetypeVersion=2.19.0 \
      -DgroupId=org.example \
      -DartifactId=word-count-beam \
      -Dversion="0.1" \
      -Dpackage=org.apache.beam.examples \
      -DinteractiveMode=false

cd word-count-beam
cat pom.xml | grep -B3 -A13 'direct-runner'
```

```
git show 561aa7b // all details
```

## ToDo

* There are no error queues at this moment and might be an interesting addition
